// simpletron.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#define READ 10
#define WRITE 11
#define LOAD 20
#define STORE 21
#define ADD 30
#define SUBTRACT 31
#define DIVIDE 32
#define MULTIPLY 33
#define BRANCH 40
#define BRANCHNEG 41
#define BRANCHZERO 42
#define HALT 43

class Computer {
	uint16_t memory[100] = { 0 };
	int pc = 0; //program counter
	int accumlator = 0;
	bool isDone = 0;
public:
	void LoadMemory(uint8_t location, uint16_t input) {
		if (location < 0 || location > 999) {
			return;
		}
		memory[location] = input;
		return;
	}
	int8_t TwoComplementsDealer(int location) {
		return (int8_t)memory[location];
	}
	uint8_t ReadMemory(uint16_t location) {
		if (location < 0 || location > 999)
			return 0;
		return memory[location];
	}
private:
	int ExecuteInstruction(int location) {
		uint8_t lsb = memory[location] / 100;
		uint8_t msb = memory[location] % 100;
		switch (lsb) {
		case READ:
			fseek(stdin, 0, SEEK_END);
			puts("Please enter in a number:");
			scanf("%d", memory[msb]);
			pc += 1;
			break;
		case WRITE:
			printf("%d", memory[msb]);
			pc += 1;
			break;
		case ADD:
			accumlator = accumlator + ReadMemory(msb);
			pc += 1;
			break;
		case LOAD:
			accumlator = ReadMemory(msb);
			pc += 1;
			break;
		case STORE:
			LoadMemory(msb, accumlator);
			pc += 1;
			break;
		case SUBTRACT:
			accumlator = accumlator - ReadMemory(msb);
			pc += 1;
			break;
		case DIVIDE:
			accumlator = accumlator / ReadMemory(msb);
			pc += 1;
			break;
		case MULTIPLY:
			accumlator = accumlator + ReadMemory(msb);
			pc += 1;
			break;
		case BRANCH:
			pc = msb;
			break;
		case BRANCHNEG:
			if (accumlator < 0) {
				pc = msb;
			}
			break;
		case BRANCHZERO:
			if (accumlator == 0) {
				pc = msb;
			}

			break;
		case HALT:
			isDone = 1;
			break;
		default:
			return 1;
			pc += 1;
			break;
		
		}
		return 0;

		}
public:
	void Run() {
		while (!isDone) {
			
			if (ExecuteInstruction(pc)) {
				printf("Invalid instruction,exiting!");
				// exit(1);
		}
	}
};
int main()
{
	int input = 0;
	printf("*** Welcome to Simpletron! ***\n");
	printf("*** Please enter your program one instruction ***\n");
	printf("*** (or data word) at a time. I will type the ***\n");
	printf("*** location number and a question mark (?).  ***\n");
	printf("*** You then type the word for that location. ***\n");
	printf("*** Type the sentinel -99999 to stop entering ***\n");
	printf("*** your program. ***\n");
	int arraycounter = 0;
	Computer com;
	while (arraycounter != 999 && input != -99999) {
		printf("%d ? ", arraycounter);
		scanf("%d", &input);
		if (input == -99999) break;
		com.LoadMemory(arraycounter, input);
		arraycounter++;

	
	}
	com.Run();

    return 0;
}

